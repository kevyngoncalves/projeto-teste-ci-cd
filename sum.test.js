const { test, expect } = require('@jest/globals');
const {sum, division, sayHello, verifyObjectIsEmpty} = require('./teste');

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1,2)).toBe(3);
})

test('divide 4 for 2 to equal 2', () => {
    expect(division(4,2)).toBe(2);
})

test('must say hello to expected name', () => {
    expect(sayHello('Dino')).toEqual('Olá, Dino');
})

test('must return false if object is not empty', () => {
    let mockObject = {
        oneItem: 'populated'
    };
    expect(verifyObjectIsEmpty(mockObject)).toBe(false);
})
test('must return true if object is empty', () => {
    let mockObject = {};
    expect(verifyObjectIsEmpty(mockObject)).toBe(true);
})