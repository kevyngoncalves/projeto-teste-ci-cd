function sum(a, b) {
    return a + b;
}

function division(a, b) {
    return a / b;
}

function sayHello(string) {
    return `Olá, ${string}`;
}

function verifyObjectIsEmpty(object) {
    for(let i in object) return false;
        return true;
}

module.exports = {sum, division, sayHello, verifyObjectIsEmpty};